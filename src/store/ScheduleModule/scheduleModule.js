// import axios from "axios";

export default {
    namespaced: true, // avoiding collisions in methods naming
    state: () => ({
        scheduleItems: [],
    }),
    getters: {
    },
    // don't call async functions in mutations cause they're sync!
    mutations: {
        updateScheduleItems(state, payload) {
            state.scheduleItems = payload
        }
    },

    actions: {
        loadEvents(context, payload) {
            //look: destructuring assignment
            let {start, end} = payload
            console.log(start, end)
            // this is stub
            // implement api-request with start, end params here
            let scheduleItems = [
                {
                    periodStart: 1641918915000,
                    periodEnd: 1641924915000,
                    periodName: "lunch time!",
                    periodType: "Lunch",
                }
            ];
            context.commit("updateScheduleItems", scheduleItems)
        }
    },
};

